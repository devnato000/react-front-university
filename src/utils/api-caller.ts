import isNil from 'lodash/isNil';
import axios, { AxiosRequestConfig, AxiosResponse, RawAxiosRequestHeaders } from 'axios';

type ExpandedAxiosRequestConfig = AxiosRequestConfig & {
  tokenObj?: any;
  path?: string;
  token?: string;
  onError?: any;
};

const getToken = () => {
  return localStorage.getItem('accessToken');
};

export const apiCaller = async (opts: ExpandedAxiosRequestConfig = {}): Promise<AxiosResponse> => {
  const { url = null, method = 'get', params = {}, data = null } = opts;
  const headers: RawAxiosRequestHeaders = {};

  headers['Content-Type'] = 'application/json';

  const token = getToken();

  if (!isNil(token)) {
    headers['Authorization'] = `Bearer ${token}`;
  }

  const requestURL = `https://university-api-nlfn.onrender.com/v1${url}`;

  const config = {
    url: requestURL,
    method,
    data,
    headers,
    params,
  };

  const axiosApiInstance = axios.create(config);

  return axiosApiInstance.request(config);
};

export const apiCallerImage = async (
  opts: ExpandedAxiosRequestConfig = {},
): Promise<AxiosResponse> => {
  const { url = null, method = 'post', params = {}, data = null } = opts;
  const headers: RawAxiosRequestHeaders = {};

  headers['Content-Type'] = 'multipart/form-data';

  const token = getToken();

  if (!isNil(token)) {
    headers['Authorization'] = `Bearer ${token}`;
  }

  const requestURL = `https://university-api-nlfn.onrender.com/v1${url}`;

  const config = {
    url: requestURL,
    method,
    data,
    headers,
    params,
  };

  const axiosApiInstance = axios.create(config);

  return axiosApiInstance.request(config);
};
