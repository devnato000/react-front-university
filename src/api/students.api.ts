import { apiCaller, apiCallerImage } from '../utils/api-caller';
import { AxiosResponse } from 'axios';

export const getStudents = async (): Promise<AxiosResponse> =>
  apiCaller({
    method: 'get',
    url: `/students`,
  });

export const getStudentsByFilters = async (sortFilter?, orderFilter?): Promise<AxiosResponse> =>
  apiCaller({
    method: 'get',
    url: `/students/sort?sortField=${sortFilter}&sortOrder=${orderFilter}`,
  });

export const getStudentsByName = async (name): Promise<AxiosResponse> =>
  apiCaller({
    method: 'get',
    url: `/students?Name=${name}`,
  });

export const postNewStudents = async (data): Promise<AxiosResponse> =>
  apiCaller({
    method: 'post',
    url: '/students',
    data: data,
  });

export const editStudent = async (id, data): Promise<AxiosResponse> =>
  apiCaller({
    method: 'patch',
    url: `/students/${id}`,
    data: data,
  });

export const addGroupToStudent = async (id, data): Promise<AxiosResponse> =>
  apiCaller({
    method: 'patch',
    url: `/students/${id}/add-group`,
    data: data,
  });

export const uploadImageStudent = async (id, data): Promise<AxiosResponse> =>
  apiCallerImage({
    method: 'post',
    url: `/students/upload/${id}`,
    data: data,
  });

export const getStudentImage = async (id): Promise<AxiosResponse> =>
  apiCallerImage({
    method: 'get',
    url: `/students/image/${id}`,
    responseType: 'arraybuffer',
  });
