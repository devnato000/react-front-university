import { apiCaller } from '../utils/api-caller';
import { AxiosResponse } from 'axios';

export const getCourses = async (): Promise<AxiosResponse> =>
  apiCaller({
    method: 'get',
    url: '/courses',
  });

export const postNewCourse = async (data): Promise<AxiosResponse> =>
  apiCaller({
    method: 'post',
    url: '/courses',
    data: data,
  });

export const editCourse = async (id, data): Promise<AxiosResponse> =>
  apiCaller({
    method: 'patch',
    url: `/courses/${id}`,
    data: data,
  });
