import { apiCaller } from '../utils/api-caller';
import { AxiosResponse } from 'axios';

export const getGroups = async (): Promise<AxiosResponse> =>
  apiCaller({
    method: 'get',
    url: '/groups',
  });

export const postNewGroup = async (data): Promise<AxiosResponse> =>
  apiCaller({
    method: 'post',
    url: '/groups',
    data: data,
  });

export const editGroup = async (id, data): Promise<AxiosResponse> =>
  apiCaller({
    method: 'patch',
    url: `/groups/${id}`,
    data: data,
  });
