import {apiCaller} from "../utils/api-caller";
import {AxiosResponse} from "axios";

export const getLectors = async (): Promise<AxiosResponse> =>
	apiCaller({
		method: 'get',
		url: '/lectors',
	});

export const getLectorById = async (id): Promise<AxiosResponse> =>
	apiCaller({
		method: 'get',
		url: `/lectors/${id}`,
	});

export const postNewLector = async (data): Promise<AxiosResponse> =>
	apiCaller({
		method: 'post',
		url: '/lectors',
		data: data,
	});

export const editLector = async (id, data): Promise<AxiosResponse> =>
	apiCaller({
		method: 'patch',
		url: `/lectors/update/${id}`,
		data: data,
	});

