import {apiCaller} from "../utils/api-caller";
import {AxiosResponse} from "axios";

export const getUsersApi = async (): Promise<AxiosResponse> =>
	apiCaller({
		method: 'get',
		url: '/lectors/me',
	});

export const signIn = async (data): Promise<AxiosResponse> =>
	apiCaller({
		method: 'post',
		url: '/auth/sign-in',
		data: data,
	});

export const signUp = async (data): Promise<AxiosResponse> =>
	apiCaller({
		method: 'post',
		url: '/auth/sign-up',
		data: data,
	});

export const requestPasswordReset = async (data): Promise<AxiosResponse> =>
	apiCaller({
		method: 'post',
		url: '/auth/reset-password-request',
		data: data,
	});

export const passwordReset = async (token, data): Promise<AxiosResponse> =>
	apiCaller({
		method: 'post',
		url: `/auth/reset-password/${token}`,
		data: data,
	});
