import React from 'react';
import './styles/index.scss'
import AppRouter from "./routes/root";
import {BrowserRouter} from "react-router-dom";
import {Toaster} from "react-hot-toast";


function App() {
	
	return (
		<div className="App">
			{/*<RouterProvider router={routerRoot}/>*/}
			<BrowserRouter>
				<AppRouter></AppRouter>
			</BrowserRouter>
			<Toaster/>
		</div>
	);
}

export default App;
