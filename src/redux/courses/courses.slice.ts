import { createSlice } from '@reduxjs/toolkit';

const coursesSlice = createSlice({
  name: 'courses',
  initialState: {
    data: [],
    currentCourseData: {
      id: 0,
      name: '',
      hours: 0,
      description: '',
    },
    dataKeys: [],
    isCourseData: false,
  },
  reducers: {
    setCourseData: (state, action) => {
      state.data = action.payload;
      state.isCourseData = true;
    },
    setCourseDataKeys: (state, action) => {
      state.dataKeys = action.payload;
    },
    setCurrentCourseData: (state, action) => {
      state.currentCourseData = action.payload;
    },
    deleteCourseData: (state) => {
      state.data = [];
      state.isCourseData = false;
    },
    deleteCourseDataKeys: (state) => {
      state.dataKeys = [];
    },
    getCoursesState: (state) => {
      return state;
    },
  },
});

export const {
  setCourseData,
  deleteCourseData,
  setCourseDataKeys,
  deleteCourseDataKeys,
  setCurrentCourseData,
} = coursesSlice.actions;
export default coursesSlice.reducer;
