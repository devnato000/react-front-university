import {createSlice} from '@reduxjs/toolkit';

const lectorsSlice = createSlice({
	name: 'lectors',
	initialState: {
		data: [],
		currentLectorData: {
			id: 0,
			name: '',
			email: '',
		},
		dataKeys: [],
		isLectorData: false,
	},
	reducers: {
		setLectorData: (state, action) => {
			state.data = action.payload;
			state.isLectorData = true
		},
		setLectorDataKeys: (state, action) => {
			state.dataKeys = action.payload;
		},
		setCurrentLectorData: (state, action) => {
			state.currentLectorData = action.payload;
		},
		deleteLectorData: (state) => {
			state.data = []
			state.isLectorData = false
		},
		deleteLectorDataKeys: (state) => {
			state.dataKeys = []
		},
		getLectorsState: (state) => {
			return state
		},
	},
});

export const {
	setLectorData,
	deleteLectorData,
	setLectorDataKeys,
	deleteLectorDataKeys,
	setCurrentLectorData,
} = lectorsSlice.actions;
export default lectorsSlice.reducer;
