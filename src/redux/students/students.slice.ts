import { createSlice } from '@reduxjs/toolkit';

const studentsSlice = createSlice({
  name: 'students',
  initialState: {
    data: [],
    currentStudentData: {
      id: 0,
      name: '',
      surname: '',
      email: '',
      group: {
        id: 0,
        name: '',
      },
      age: 0,
      imagePath: '',
    },
    dataKeys: [],
    isStudentData: false,
  },
  reducers: {
    setStudentData: (state, action) => {
      state.data = action.payload;
      state.isStudentData = true;
    },
    setStudentDataKeys: (state, action) => {
      state.dataKeys = action.payload;
    },
    setCurrentStudentData: (state, action) => {
      state.currentStudentData = action.payload;
    },
    deleteStudentData: (state) => {
      state.data = [];
      state.isStudentData = false;
    },
    deleteStudentDataKeys: (state) => {
      state.dataKeys = [];
    },
    getStudentsState: (state) => {
      return state;
    },
  },
});

export const {
  setStudentData,
  deleteStudentData,
  setStudentDataKeys,
  deleteStudentDataKeys,
  setCurrentStudentData,
} = studentsSlice.actions;
export default studentsSlice.reducer;
