import { configureStore } from '@reduxjs/toolkit';
import authStateSlice from './authState/authState.slice';
import lectorsSlice from './lectors/lectors.slice';
import modalSlice from './modalState/modal.slice';
import coursesSlice from './courses/courses.slice';
import studentsSlice from './students/students.slice';
import groupsSlice from './groups/groups.slice';

export const store = configureStore({
  reducer: {
    authState: authStateSlice,
    lectors: lectorsSlice,
    courses: coursesSlice,
    students: studentsSlice,
    groups: groupsSlice,
    modal: modalSlice,
  },
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
