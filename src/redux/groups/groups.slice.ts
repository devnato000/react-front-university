import { createSlice } from '@reduxjs/toolkit';

const groupsSlice = createSlice({
  name: 'groups',
  initialState: {
    data: [],
    currentGroupData: {
      id: 0,
      name: '',
    },
    dataKeys: [],
    isGroupData: false,
  },
  reducers: {
    setGroupData: (state, action) => {
      state.data = action.payload;
      state.isGroupData = true;
    },
    setGroupDataKeys: (state, action) => {
      state.dataKeys = action.payload;
    },
    setCurrentGroupData: (state, action) => {
      state.currentGroupData = action.payload;
    },
    deleteGroupData: (state) => {
      state.data = [];
      state.isGroupData = false;
    },
    deleteGroupDataKeys: (state) => {
      state.dataKeys = [];
    },
    getGroupsState: (state) => {
      return state;
    },
  },
});

export const {
  setGroupData,
  deleteGroupData,
  setGroupDataKeys,
  deleteGroupDataKeys,
  setCurrentGroupData,
} = groupsSlice.actions;
export default groupsSlice.reducer;
