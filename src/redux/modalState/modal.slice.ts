import {createSlice} from '@reduxjs/toolkit';

const modalSlice = createSlice({
	name: 'modal',
	initialState: {
		isEditModalOpen: false,
		modalData: []
	},
	reducers: {
		setEditModalState: (state, action) => {
			state.isEditModalOpen = action.payload;
		},
		setModalData: (state, action) => {
			state.modalData = action.payload;
		}
	},
});

export const {setEditModalState} = modalSlice.actions;
export default modalSlice.reducer;
