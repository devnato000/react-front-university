import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { LoginPage } from '../components/pages/LoginPage/LoginPage';
import { RegisterPage } from '../components/pages/RegisterPage/RegisterPage';
import { ForgotPassPage } from '../components/pages/ForgotPassPage/ForgotPassPage';
import { ResetPassPage } from '../components/pages/ResetPassPage/ResetPassPage';
import { DashboardPage } from '../components/pages/Dashboard/DashboardPage/DashboardPage';
import { CoursesPage } from '../components/pages/Dashboard/CoursesPage/CoursesPage';
import { LectorsPage } from '../components/pages/Dashboard/LectorsPage/LectorsPage';
import { GroupsPage } from '../components/pages/Dashboard/GroupsPage/GroupsPage';
import { StudentsPage } from '../components/pages/Dashboard/StudentsPage/StudentsPage';
import { DashboardPageLayout } from '../components/pageLayouts/DashboardPageLayout/DashboardPageLayout';
import { ProfilePage } from '../components/pages/Profile/ProfilePage';

export default function AppRouter() {
  const dashboardPageProps = { title: 'dashboard' };
  const coursesPageProps = { title: 'courses' };
  const lectorsPageProps = { title: 'lectors' };
  const groupsPageProps = { title: 'groups' };
  const studentsPageProps = { title: 'students' };

  return (
    <>
      <Routes>
        <Route path='/' element={<LoginPage />} />
        <Route path='/sign-up' element={<RegisterPage />} />
        <Route path='/forgot-password' element={<ForgotPassPage />} />
        <Route path='/reset-password/:token' element={<ResetPassPage />} />
        <Route path='/university' element={<DashboardPageLayout />}>
          <Route path='dashboard' element={<DashboardPage {...dashboardPageProps} />} />
          <Route path='courses' element={<CoursesPage {...coursesPageProps} />} />
          <Route path='lectors' element={<LectorsPage {...lectorsPageProps} />} />
          <Route path='groups' element={<GroupsPage {...groupsPageProps} />} />
          <Route path='students' element={<StudentsPage {...studentsPageProps} />} />
        </Route>
        <Route path='/profile/:id' element={<ProfilePage />} />
      </Routes>
    </>
  );
}
