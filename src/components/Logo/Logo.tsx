import React from 'react';

interface ILogoProps {
	type: 'Color' | 'Mono';
}

enum types {
	color = 'Color',
	mono = 'Mono'
}

export const Logo: React.FC<ILogoProps> = (type) => {
	
	return (
		<>
			{FindLogo(type)}
		</>
	)
}

const FindLogo = (type: ILogoProps) => {
	if (type.type === types.color) {
		return (<LogoColor/>)
	} else if (type.type === types.mono) {
		return (<LogoMono/>)
	}
}

const LogoColor = () => {
	return (
		<>
			<svg width="79" height="80" viewBox="0 0 79 80" fill="none"
					 xmlns="http://www.w3.org/2000/svg">
				<path d="M39.5 45.7333L19.7722 57.1556L0 45.7333L19.7722 34.3111L39.5 45.7333Z"
							fill="#D5B5FF"/>
				<path d="M0 45.7333V68.5777L19.7722 80V57.1556L0 45.7333Z" fill="#A965FF"/>
				<path d="M39.5001 45.7333V68.5777L19.7722 80V57.1556L39.5001 45.7333Z" fill="#7101FF"/>
				<path d="M79 45.7333L59.2278 57.1556L39.5 45.7333L59.2278 34.3111L79 45.7333Z"
							fill="#D5B5FF"/>
				<path d="M39.5 45.7333V68.5777L59.2278 80V57.1556L39.5 45.7333Z" fill="#A965FF"/>
				<path d="M78.9999 45.7333V68.5777L59.2278 80V57.1556L78.9999 45.7333Z" fill="#7101FF"/>
				<path d="M59.2279 11.4222L39.5001 22.8444L19.7722 11.4222L39.5001 0L59.2279 11.4222Z"
							fill="#D5B5FF"/>
				<path d="M19.7722 11.4222V34.3111L39.5001 45.7333V22.8444L19.7722 11.4222Z" fill="#A965FF"/>
				<path d="M59.2278 11.4222V34.3111L39.5 45.7333V22.8444L59.2278 11.4222Z" fill="#7101FF"/>
			</svg>
		</>
	)
}

const LogoMono = () => {
	return (
		<>
			<svg width="42" height="43" viewBox="0 0 42 43" fill="none"
					 xmlns="http://www.w3.org/2000/svg">
				<path d="M21 24.5816L10.5118 30.7211L0 24.5816L10.5118 18.4422L21 24.5816Z" fill="white"/>
				<path d="M0 24.5816V36.8605L10.5118 43V30.7211L0 24.5816Z" fill="#DBD2E6"/>
				<path d="M21 24.5816V36.8605L10.5118 43V30.7211L21 24.5816Z" fill="#B4A1C8"/>
				<path d="M42 24.5816L31.4882 30.7211L21 24.5816L31.4882 18.4422L42 24.5816Z" fill="white"/>
				<path d="M21 24.5816V36.8605L31.4882 43V30.7211L21 24.5816Z" fill="#DBD2E5"/>
				<path d="M42 24.5816V36.8605L31.4882 43V30.7211L42 24.5816Z" fill="#B4A1C8"/>
				<path d="M31.4882 6.13943L21 12.2789L10.5118 6.13943L21 0L31.4882 6.13943Z" fill="white"/>
				<path d="M10.5118 6.13943V18.4422L21 24.5816V12.2789L10.5118 6.13943Z" fill="#DBD2E5"/>
				<path d="M31.4882 6.13943V18.4422L21 24.5816V12.2789L31.4882 6.13943Z" fill="#B4A1C8"/>
			</svg>
		</>
	)
}