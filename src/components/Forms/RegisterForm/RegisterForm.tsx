import React from 'react';
import { AppButton } from '../../components/Buttons/Button';
import { FormInput } from '../../components/Inputs/FormInput/FormInput';
import { Checkbox } from '../../components/Checkbox/Checkbox';
import { useForm } from 'react-hook-form';
import { signUp } from '../../../api/auth.api';
import { useNavigate } from 'react-router-dom';

export interface IRegisterForm {
  buttonTitle: string;
}

interface RegisterFormInputs {
  email: string;
  password: string;
  confirmPassword?: string;
}

export const RegisterForm: React.FC<IRegisterForm> = ({ buttonTitle }) => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<RegisterFormInputs>({ mode: 'all' });

  const navigate = useNavigate();

  const password = watch('password');

  const onSubmit = async (data: RegisterFormInputs) => {
    try {
      delete data.confirmPassword;
      const response = await signUp(data);
      localStorage.setItem('accessToken', response.data.accessToken);
      navigate('/');
    } catch (error) {
      console.error(error);
    }
  };
  const [shown, setShown] = React.useState(false);

  const isShown = shown ? 'text' : 'password';

  return (
    <form onSubmit={handleSubmit(onSubmit)} className='auth_form'>
      <FormInput
        label='email'
        type='email'
        placeholder='example@mail.com'
        register={register('email', {
          required: {
            value: true,
            message: `This field is required`,
          },
          pattern: {
            value: /(\w\.?)+@[\w.-]+\.\w{2,4}/,
            message: `Please enter valid email`,
          },
        })}
        error={errors.email && errors.email?.message}
      />
      <FormInput
        label='password'
        type={isShown}
        placeholder='password'
        register={register('password', {
          required: {
            value: true,
            message: `This field is required`,
          },
        })}
        error={errors.password && errors.password?.message}
      />
      <FormInput
        label='Confirm password'
        type='password'
        placeholder='confirm password'
        register={register('confirmPassword', {
          required: {
            value: true,
            message: `This field is required`,
          },
          validate: (value) => value === password || 'Passwords do not match',
        })}
        error={errors.confirmPassword && errors.confirmPassword?.message}
      />
      <Checkbox label='show password' onChange={() => setShown(!shown)} />
      <AppButton title={buttonTitle} type='submit' />
    </form>
  );
};
