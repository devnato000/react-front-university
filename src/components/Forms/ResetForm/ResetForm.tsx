import React, { useState } from 'react';
import { AppButton } from '../../components/Buttons/Button';
import { FormInput } from '../../components/Inputs/FormInput/FormInput';
import { useForm } from 'react-hook-form';
import { Checkbox } from '../../components/Checkbox/Checkbox';
import { isPasswordValid } from '../../../utils/validation-schema';
import { useNavigate } from 'react-router-dom';
import { passwordReset } from '../../../api/auth.api';

export interface IResetForm {
  buttonTitle: string;
}

interface IFormFields {
  email: string;
  newPassword: string;
  confirmPassword?: string;
}

export const ResetForm: React.FC<IResetForm> = ({ buttonTitle }) => {
  const [formStep, setFormStep] = useState(0);

  const navigate = useNavigate();

  const completeFormStep = () => {
    setFormStep((step) => step + 1);
  };

  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
    watch,
  } = useForm<IFormFields>({ mode: 'all' });

  const renderButton = () => {
    if (formStep === 1) {
      return <AppButton title='Login in' type='button' onClick={() => navigate('/')} />;
    } else {
      return <AppButton disabled={!isValid} title={buttonTitle} type='submit' />;
    }
  };

  const password = watch('newPassword');

  const token = window.location.pathname.split('/').splice(2, 1);
  const onSubmit = (data: IFormFields) => {
    delete data.confirmPassword;
    passwordReset(token[0], data);
    completeFormStep();
  };

  const [shown, setShown] = React.useState(false);
  const isShown = shown ? 'text' : 'password';

  return (
    <form onSubmit={handleSubmit(onSubmit)} className='auth_form'>
      {formStep === 0 && (
        <>
          <FormInput
            label='new password'
            type={isShown}
            id='password'
            placeholder='new password'
            register={register('newPassword', isPasswordValid)}
            error={errors.newPassword && errors.newPassword?.message}
          />
          <FormInput
            label='confirm password'
            type='password'
            placeholder='confirm password'
            register={register('confirmPassword', {
              required: {
                value: true,
                message: `This field is required`,
              },
              validate: (value) => value === password || 'Passwords do not match',
            })}
            error={errors.confirmPassword && errors.confirmPassword?.message}
          />
          <Checkbox label='show password' onChange={() => setShown(!shown)} />
          {renderButton()}
        </>
      )}
      {formStep === 1 && (
        <>
          <p className='form-message form-message--center'>
            You can use your new password to log into your account
          </p>
          {renderButton()}
        </>
      )}
    </form>
  );
};
