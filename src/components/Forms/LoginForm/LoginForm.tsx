import React from 'react';
import { AppButton } from '../../components/Buttons/Button';
import { FormInput } from '../../components/Inputs/FormInput/FormInput';
import { Checkbox } from '../../components/Checkbox/Checkbox';
import { useForm } from 'react-hook-form';
import { isEmailValid, isPasswordValid } from '../../../utils/validation-schema';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch } from '../../../utils/hooks';
import { getUsersApi, signIn } from '../../../api/auth.api';
import { login } from '../../../redux/authState/authState.slice';
import { AppLink } from '../../components/Links/Link';
import styles from './styles.module.scss';
import toast from 'react-hot-toast';

export interface ILoginForm {
  buttonTitle: string;
}

interface LoginFormInputs {
  email: string;
  password: string;
}

export const LoginForm: React.FC<ILoginForm> = ({ buttonTitle }) => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginFormInputs>({ mode: 'all' });
  const notify = (error) => toast.error(`${error}`);

  const onSubmit = async (data: LoginFormInputs) => {
    try {
      const response = await signIn(data);
      localStorage.setItem('accessToken', response.data.accessToken);
      const user = await getUsersApi();
      if (user) {
        dispatch(login(user.data));
      }
      navigate('/university/dashboard');
    } catch (error: any) {
      notify(error.response?.data?.message);
    }
  };
  const [shown, setShown] = React.useState(false);

  const isShown = shown ? 'text' : 'password';

  return (
    <form onSubmit={handleSubmit(onSubmit)} className='auth_form'>
      <FormInput
        label='email'
        type='email'
        placeholder='example@mail.com'
        register={register('email', isEmailValid)}
        error={errors.email && errors.email?.message}
      />
      <FormInput
        label='password'
        type={isShown}
        placeholder='password'
        register={register('password', isPasswordValid)}
        error={errors.password && errors.password?.message}
      />
      <Checkbox label='show password' onChange={() => setShown(!shown)} />
      <AppButton title={buttonTitle} type='submit' />
      <div className={styles['login-form__help']}>
        <AppLink to='/forgot-password' title='Forgot password?' />
        <AppLink to='/sign-up' title='Dont have account?' />
      </div>
    </form>
  );
};
