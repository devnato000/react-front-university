import React from 'react';
import { FormInput } from '../../components/Inputs/FormInput/FormInput';
import { useForm } from 'react-hook-form';
import { AppLink } from '../../components/Links/Link';
import { isEmailValid } from '../../../utils/validation-schema';
import { requestPasswordReset } from '../../../api/auth.api';
import toast from 'react-hot-toast';
import { AppButton } from '../../components/Buttons/Button';

export interface IForgotPassForm {
  buttonTitle: string;
  linkTitle: string;
}

interface IFormFields {
  email: string;
  newPassword: string;
  confirmPassword: string;
}

export const ForgotPassForm: React.FC<IForgotPassForm> = ({ buttonTitle, linkTitle }) => {
  const notifyError = (error) => toast.error(`${error}`);
  const notify = (data) => toast.success(`Request ${data}`);

  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<IFormFields>({ mode: 'all' });

  const onSubmit = async (data: IFormFields) => {
    try {
      const resetRequest = await requestPasswordReset(data);
      notify(resetRequest.statusText);
    } catch (error: any) {
      notifyError(error.response?.data?.message);
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className='auth_form'>
      <p className='form-message'>
        Don`t worry, happens to the best of us. Enter the email address associated with your account
        and we`ll send you a link to reset.
      </p>
      <FormInput
        label='email'
        type='email'
        placeholder='name@mail.com'
        register={register('email', isEmailValid)}
        error={errors.email && errors.email?.message}
      />
      <AppButton disabled={!isValid} title={buttonTitle} type='submit' />
      <AppLink to='/' title={linkTitle} />
    </form>
  );
};
