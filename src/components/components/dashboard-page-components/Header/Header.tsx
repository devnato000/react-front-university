import React from 'react';
import header from './header.module.scss';
import avatar from '../../../../images/avatar.png';
import { Link } from 'react-router-dom';
import { Logo } from '../../../Logo/Logo';

interface IHeaderProps {
  title?: string;
}

export const AppHeader: React.FC<IHeaderProps> = ({ title }) => {
  return (
    <header className={header.header}>
      {title ? (
        <h1 className={header['header__title']}>{title}</h1>
      ) : (
        <Link to='/university/dashboard'>
          <Logo type='Color' />
        </Link>
      )}
      <div className={header['header-wrapper']}>
        <Link to='#'>
          <img src={avatar} alt='avatar' />
        </Link>
      </div>
    </header>
  );
};
