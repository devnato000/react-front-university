import React, { useEffect, useState } from 'react';
import aside from './aside.module.scss';
import { Logo } from '../../../Logo/Logo';
import dashboardIcon from '../../../../images/dashboard-icons/dashboard.svg';
import coursesIcon from '../../../../images/dashboard-icons/courses.svg';
import lectorIcon from '../../../../images/dashboard-icons/lector.svg';
import groupsIcon from '../../../../images/dashboard-icons/groups.svg';
import studentsIcon from '../../../../images/dashboard-icons/student.svg';
import logoutIcon from '../../../../images/dashboard-icons/logout.svg';
import classNames from 'classnames';
import { useDispatch } from 'react-redux';
import { logout } from '../../../../redux/authState/authState.slice';
import { Link } from 'react-router-dom';

export const DashboardAside = () => {
  const dispatch = useDispatch();
  const [activeTab, setActiveTab] = useState('/university');

  const handleTabClick = (tabIndex: string) => {
    setActiveTab(tabIndex);
  };

  const logOut = () => {
    localStorage.clear();
    dispatch(logout());
  };

  useEffect(() => {
    const currentLocation = window.location.pathname;
    setActiveTab(currentLocation);
  });

  return (
    <aside className={aside.aside}>
      <div className={aside['aside-logo-wrapper']}>
        <Logo type={'Mono'} />
      </div>
      <nav className={aside.aside__nav}>
        <ul className={aside['aside-nav__list']}>
          <li>
            <Link
              to='dashboard'
              className={classNames(aside['aside-nav-list__link'], {
                [aside['aside-nav-list__link--active']]: activeTab === '/university/dashboard',
              })}
              onClick={() => handleTabClick('/university/dashboard')}
            >
              <img src={dashboardIcon} className={aside['aside-nav-list-link__icon']} alt='' />
              dashboard
            </Link>
          </li>
          <li>
            <Link
              to='courses'
              className={classNames(aside['aside-nav-list__link'], {
                [aside['aside-nav-list__link--active']]: activeTab === '/university/courses',
              })}
              onClick={() => handleTabClick('/university/courses')}
            >
              <img src={coursesIcon} className={aside['aside-nav-list-link__icon']} alt='' />
              courses
            </Link>
          </li>
          <li>
            <Link
              to='lectors'
              className={classNames(aside['aside-nav-list__link'], {
                [aside['aside-nav-list__link--active']]: activeTab === '/university/lectors',
              })}
              onClick={() => handleTabClick('/university/lectors')}
            >
              <img src={lectorIcon} className={aside['aside-nav-list-link__icon']} alt='' />
              lectors
            </Link>
          </li>
          <li>
            <Link
              to='groups'
              className={classNames(aside['aside-nav-list__link'], {
                [aside['aside-nav-list__link--active']]: activeTab === '/university/groups',
              })}
              onClick={() => handleTabClick('/university/groups')}
            >
              <img src={groupsIcon} className={aside['aside-nav-list-link__icon']} alt='' />
              groups
            </Link>
          </li>
          <li>
            <Link
              to='students'
              className={classNames(aside['aside-nav-list__link'], {
                [aside['aside-nav-list__link--active']]: activeTab === '/university/students',
              })}
              onClick={() => handleTabClick('/university/students')}
            >
              <img src={studentsIcon} className={aside['aside-nav-list-link__icon']} alt='' />
              students
            </Link>
          </li>
          <li>
            <Link to='/' onClick={logOut} className={classNames(aside['aside-nav-list__link'])}>
              <img src={logoutIcon} className={aside['aside-nav-list-link__icon']} alt='' />
              Log out
            </Link>
          </li>
        </ul>
      </nav>
    </aside>
  );
};
