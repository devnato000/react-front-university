import React, { ReactNode } from 'react';
import main from './main.module.scss';

interface IDashboardProps {
  children: ReactNode;
}

export interface IButtonProps {
  title: string;
  type: 'button' | 'submit' | 'reset' | undefined;
  onClick: () => void;
}

export const DashboardMain: React.FC<IDashboardProps> = ({ children }) => {
  const childArray = React.Children.toArray(children);
  const [firstChild, secondChild] = childArray;

  if (childArray.length !== 2) {
    return (
      <main className={main.wrapper}>
        <div className={main['dashboard-controls']}></div>
        <div className={main['main-wrapper']}>{firstChild}</div>
      </main>
    );
  }

  return (
    <main className={main.wrapper}>
      <div className={main['dashboard-controls']}>{firstChild}</div>
      <div className={main['main-wrapper']}>{secondChild}</div>
    </main>
  );
};
