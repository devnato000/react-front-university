import React from 'react';
import modal from './modal.module.scss';

interface ModalProps {
	isOpen: boolean;
	onClose: () => void;
	children: React.ReactNode
}

const Modal: React.FC<ModalProps> = ({isOpen, onClose, children}) => {
	if (!isOpen) return null;
	return (
		isOpen && (
			<div className={modal["modal-overlay"]}>
				<div className={modal["modal"]}>
					<button className={modal["modal-close"]} onClick={onClose}>
						&times;
					</button>
					<div className={modal["modal-content"]}>{children}</div>
				</div>
			</div>
		)
	);
};

export default Modal;
