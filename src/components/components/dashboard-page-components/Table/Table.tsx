import React from 'react';
import mainRow from '../Main/mainRow.module.scss';
import main from '../Main/main.module.scss';

export interface ITableProps {
  header: { name: string }[];
  gridTemplate: string;
  children: React.ReactNode;
}

export const AppTable: React.FC<ITableProps> = ({ header, gridTemplate, children }) => {
  const rowStyles = {
    gridTemplateColumns: gridTemplate,
  };

  return (
    <>
      <div className={mainRow['main__titles-row']} style={rowStyles}>
        {header.map((item) => {
          return (
            <h3 className={mainRow['main-titles-row__title']} key={item.name}>
              {item.name}
            </h3>
          );
        })}
      </div>
      <div className={main['main-row-wrapper']}>{children}</div>
    </>
  );
};
