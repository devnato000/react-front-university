import React from 'react';
import styles from "../Checkbox/Checkbox.module.scss"

interface IPasswordInput {
	label: string
	onChange: () => void
}

export const Checkbox: React.FC<IPasswordInput> = ({label, onChange}) => {
	
	return (
		<>
			<label className={styles.checkbox__label}>{label}
				<input className={styles.input} onChange={onChange}
							 type="checkbox"/>
			</label>
		</>
	)
}