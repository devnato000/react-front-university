import React from 'react';
import Select, { StylesConfig } from 'react-select';

const profileStyles: StylesConfig<Option, true> = {
  control: (provided) => ({
    ...provided,
    backgroundColor: 'white',
    borderColor: 'lightgray',
    borderRadius: '0',
    fontSize: '14px',
    height: '48px',
    minWidth: '375px',
    flexWrap: 'nowrap',
    '&:hover': {
      borderColor: '#7101FF',
      boxShadow: '0 0 0 1px #7101FF',
    },
  }),
  valueContainer: (provided) => ({
    ...provided,
    maxWidth: '300px',
    overflow: 'hidden',
    flexWrap: 'nowrap',
  }),
  multiValue: (provided) => ({
    ...provided,
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: '0',
  }),
  multiValueLabel: (provided) => ({
    ...provided,
    fontSize: '14px',
    padding: '5px 0 5px 5px',
    fontWeight: '400',
    color: '#000',
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: 'none',
  }),
  multiValueRemove: (provided) => ({
    ...provided,
    color: '#000',
    padding: '2px',
    '&:hover': {
      backgroundColor: 'transparent',
      color: 'red',
    },
  }),
  menu: (provided) => ({
    ...provided,
    fontSize: '14px',
    fontWeight: '400',
    color: '#000',
    borderRadius: '0px',
    padding: '0',
    margin: '0',
  }),
  option: (provided, state) => ({
    ...provided,
    '&': {
      color: state.isSelected ? 'white' : 'black',
      backgroundColor: state.isSelected ? 'rgba(113,1,255,0.9)' : 'white',
    },
    '&:hover': {
      color: 'white',
      backgroundColor: 'rgba(113,1,255,1)',
    },
  }),
};

const dashboardStyles: StylesConfig<Option, true> = {
  control: (provided) => ({
    ...provided,
    backgroundColor: 'white',
    borderColor: 'lightgray',
    borderRadius: '6px',
    fontSize: '14px',
    height: '38px',
    minWidth: '140px',
    maxWidth: '500px',
    flexWrap: 'nowrap',
    '&:hover': {
      borderColor: '#7101FF',
      boxShadow: '0 0 0 1px #7101FF',
    },
  }),
  valueContainer: (provided) => ({
    ...provided,
    flexWrap: 'nowrap',
  }),
  multiValue: (provided) => ({
    ...provided,
    backgroundColor: '#7101FF',
    borderRadius: '4px',
  }),
  multiValueLabel: (provided) => ({
    ...provided,
    fontSize: '14px',
    fontWeight: '400',
    color: '#FFF',
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: 'none',
  }),
  multiValueRemove: (provided) => ({
    ...provided,
    borderRadius: '4px',
    color: 'white',
    '&:hover': {
      backgroundColor: 'red',
      color: 'white',
    },
  }),
  menu: (provided) => ({
    ...provided,
    fontSize: '14px',
    fontWeight: '400',
    color: '#000',
  }),
  option: (provided) => ({
    ...provided,
    '&:target': {
      backgroundColor: 'rgba(113,1,255,0.7)',
    },
    '&:hover': {
      backgroundColor: 'rgba(113,1,255,0.8)',
    },
  }),
};

export interface Option {
  value: number | string | string[];
  label: string;
}

export interface IAppSelectProps {
  isMulti?: true | undefined;
  options: Option[];
  styles: 'dashboard' | 'profile' | null;
  className?: string;
  onChange?: (value) => any;
  inputRef?: any;
}

export const AppSelect: React.FC<IAppSelectProps> = (AppSelectProps) => {
  const { isMulti, options, styles, className, onChange, inputRef } = AppSelectProps;

  return (
    <>
      <Select
        ref={inputRef}
        className={className}
        styles={
          styles === 'dashboard'
            ? { ...dashboardStyles }
            : styles === 'profile'
            ? { ...profileStyles }
            : { ...dashboardStyles }
        }
        closeMenuOnSelect={false}
        defaultValue={null}
        isMulti={isMulti}
        onChange={onChange}
        isClearable
        options={options}
      />
    </>
  );
};
