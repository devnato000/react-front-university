import React from 'react';
import styles from "./Link.module.scss"
import {Link} from "react-router-dom";


interface ILinkProps {
	to: string
	title: string
	
}

export const AppLink: React.FC<ILinkProps> = ({to, title}) => {
	
	return (
		<>
			<Link to={to} className={styles.link}>{title}</Link>
		</>
	)
}