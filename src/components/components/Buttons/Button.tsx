import React from 'react';
import styles from './Button.module.scss';
import classNames from 'classnames';

interface ButtonProps {
  disabled?: boolean;
  onClick?: () => void;
  title: string;
  type: 'button' | 'submit' | 'reset' | undefined;
  buttonClass?: string;
}

export const AppButton: React.FC<ButtonProps> = ({
  disabled,
  onClick,
  title,
  type,
  buttonClass,
}) => {
  const buttonClasses = classNames(styles.button, buttonClass);
  return (
    <>
      <button disabled={disabled} onClick={onClick} className={buttonClasses} type={type}>
        {title}
      </button>
    </>
  );
};
