import React from 'react';
import styles from './Input.module.scss';
import { UseFormRegisterReturn } from 'react-hook-form';
import SearchIcon from '../../../../images/dashboard-icons/search-normal.svg';

export interface ISearchInputProps {
  label?: string;
  type: string;
  id?: string;
  placeholder: string;
  register?: UseFormRegisterReturn;
  error?: string;
  onChange?: (e: any) => void;
}

export const SearchInput: React.FC<ISearchInputProps> = ({
  label,
  type,
  id,
  placeholder,
  register,
  error,
  onChange,
}) => {
  return (
    <>
      <label htmlFor={id} className={styles.input_label}>
        {label}
        <input
          id={id}
          className={styles.input}
          type={`${type}`}
          placeholder={placeholder}
          {...register}
          onChange={onChange}
        />
        {error && <span className='error-message'>{error}</span>}
        <img className={styles.input_icon} src={SearchIcon} alt='' />
      </label>
    </>
  );
};
