import React from 'react';
import styles from './Input.module.scss';
import { UseFormRegisterReturn } from 'react-hook-form';
import classNames from 'classnames';

interface IinputProps {
  label?: string;
  type: string;
  id?: string;
  placeholder: string;
  value?: string | number;
  defaultValue?: string | number;
  register?: UseFormRegisterReturn;
  error?: string;
  accept?: string;
  labelClass?: string;
  inputClass?: string;
}

export const FormInput: React.FC<IinputProps> = ({
  label,
  type,
  id,
  placeholder,
  register,
  error,
  value,
  defaultValue,
  accept,
  labelClass,
  inputClass,
}) => {
  const labelClasses = classNames(labelClass, styles.input_label);
  const inputClasses = classNames(inputClass, styles.input);

  return (
    <>
      <label htmlFor={id} className={labelClasses}>
        {label}
        <input
          id={id}
          className={inputClasses}
          type={`${type}`}
          placeholder={placeholder}
          accept={accept}
          value={value}
          onChange={(e) => e.target.value}
          defaultValue={defaultValue}
          {...register}
        />
        {error && <span className='error-message'>{error}</span>}
      </label>
    </>
  );
};
