import React, { useEffect } from 'react';
import page from './page.module.scss';
import { DashboardAside } from '../../components/dashboard-page-components/Aside/Aside';
import { getUsersApi } from '../../../api/auth.api';
import { RootState } from '../../../redux/store';
import { useAppDispatch, useAppSelector } from '../../../utils/hooks';
import { useNavigate, Outlet } from 'react-router-dom';
import toast from 'react-hot-toast';
import { login, logout } from '../../../redux/authState/authState.slice';

interface IDashboardLayoutProps {
  title?: string;
  children?: React.ReactNode | React.ReactNode[];
}

export const DashboardPageLayout: React.FC<IDashboardLayoutProps> = () => {
  const isAuthenticated = useAppSelector((state: RootState) => state.authState.isAuthenticated);

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const notify = (error) => toast.error(`${error}`);
  const expired = () =>
    toast('Your token expired.\n Please login again', {
      duration: 5000,
    });

  const loadUserFromLocalStorage = async () => {
    try {
      const user = await getUsersApi();
      return user;
    } catch (error) {
      return;
    }
  };

  const redirectByAuth = () => {
    if (isAuthenticated) {
      navigate('/university');
    } else {
      navigate('/');
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const userResponse = await loadUserFromLocalStorage();
        if (userResponse) {
          dispatch(login(userResponse.data));
        } else {
          localStorage.clear();
          dispatch(logout());
          redirectByAuth();
          expired();
        }
      } catch (error) {
        notify(error);
      }
    };

    fetchData();
  }, [navigate]);

  return (
    <>
      <div className={page.wrapper}>
        <DashboardAside />
        <div className={page['main-wrapper']}>
          <Outlet />
        </div>
      </div>
    </>
  );
};
