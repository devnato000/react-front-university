import React from 'react';
import {ILoginForm, LoginForm} from "../../Forms/LoginForm/LoginForm";
import AuthLayout from "../../pageLayouts/AuthPageLayout/AuthPageLayout";


export const LoginPage = () => {
	const LoginPageProps: ILoginForm = {
		buttonTitle: 'Login'
	}
	const title = 'Welcome!'
	return (
		<AuthLayout title={title}>
			<LoginForm {...LoginPageProps}/>
		</AuthLayout>
	)
}