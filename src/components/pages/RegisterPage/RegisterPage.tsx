import React from 'react';
import {IRegisterForm, RegisterForm} from "../../Forms/RegisterForm/RegisterForm";
import AuthLayout from "../../pageLayouts/AuthPageLayout/AuthPageLayout";


export const RegisterPage = () => {
	const RegisterPageProps: IRegisterForm = {
		buttonTitle: 'Register'
	}
	const title = 'Register your account'
	
	return (
		<AuthLayout title={title}>
			<RegisterForm {...RegisterPageProps}/>
		</AuthLayout>
	)
}