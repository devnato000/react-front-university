import React from 'react';
import {ForgotPassForm, IForgotPassForm} from "../../Forms/ForgotPassForm/ForgotPassForm";
import AuthLayout from "../../pageLayouts/AuthPageLayout/AuthPageLayout";


export const ForgotPassPage = () => {
	const ForgotPassPageProps: IForgotPassForm = {
		buttonTitle: 'Reset',
		linkTitle: 'Cancel'
	}
	const title = 'Reset Password'
	
	return (
		<AuthLayout title={title}>
			<ForgotPassForm {...ForgotPassPageProps}/>
		</AuthLayout>
	)
}