import React, { useState } from 'react';
import toast from 'react-hot-toast';
import { AppHeader } from '../../../components/dashboard-page-components/Header/Header';
import {
  DashboardMain,
  IButtonProps,
} from '../../../components/dashboard-page-components/Main/DashboardMain';
import Modal from '../../../components/dashboard-page-components/Modal/Modal';
import modal from '../../../components/dashboard-page-components/Modal/modal.module.scss';
import { FormInput } from '../../../components/Inputs/FormInput/FormInput';
import { isEmailValid, isPasswordValid } from '../../../../utils/validation-schema';
import { useForm } from 'react-hook-form';
import { LectorResponse } from './LectorsResponse';
import { postNewLector } from '../../../../api/lectors.api';
import { AppButton } from '../../../components/Buttons/Button';
import main from '../../../components/dashboard-page-components/Main/main.module.scss';
import { AppSelect, Option } from '../../../components/Select/AppSelect';
import { ISearchInputProps, SearchInput } from '../../../components/Inputs/SearchInput/SearchInput';

interface ILectorsPageProps {
  title: string;
}

export interface IRowLectorsData {
  id: number;
  name: string;
  email: string;
}

export interface IAddLectorFormInputs {
  name: string;
  email: string;
  password: string;
}

export const LectorsPage: React.FC<ILectorsPageProps> = (LectorsPageProps) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<IAddLectorFormInputs>({ mode: 'all' });

  const openModal = () => {
    setIsModalOpen(true);
  };
  const closeModal = () => {
    setIsModalOpen(false);
    reset();
  };

  const inputProps: ISearchInputProps = {
    type: 'search',
    id: 'search',
    placeholder: 'Search',
  };
  const buttonProps: IButtonProps = {
    title: '+ Add new Lector',
    type: 'submit',
    onClick: openModal,
  };

  const notify = (error) => toast.error(`${error}`);

  const onSubmit = async (data: IAddLectorFormInputs) => {
    try {
      await postNewLector(data);
      reset();
    } catch (error: any) {
      notify(error.response?.data?.message);
    }
  };
  const options: Option[] = [
    { value: 1, label: 'Option 1' },
    { value: 2, label: 'Option 2' },
  ];
  return (
    <>
      <AppHeader title={LectorsPageProps.title} />
      <DashboardMain>
        <>
          <label htmlFor='sort' className={main['dashboard-controls__label']}>
            sort by
          </label>
          <AppSelect styles='dashboard' isMulti options={options} />
          <SearchInput {...inputProps} />
          <AppButton
            title={buttonProps.title}
            type={buttonProps.type}
            onClick={buttonProps.onClick}
          />
        </>
        <LectorResponse event={onSubmit} />
      </DashboardMain>
      <Modal isOpen={isModalOpen} onClose={closeModal}>
        <form onSubmit={handleSubmit(onSubmit)} className={modal['modal-form']}>
          <h2 className={modal['modal-title']}>Add New Lector</h2>
          <FormInput
            label='name'
            type='text'
            placeholder='John'
            register={register('name', {
              required: {
                value: true,
                message: 'This field is required',
              },
            })}
            error={errors.name && errors.name?.message}
          />
          <FormInput
            label='email'
            type='email'
            placeholder='example@mail.com'
            register={register('email', isEmailValid)}
            error={errors.email && errors.email?.message}
          />
          <FormInput
            label='password'
            type='password'
            placeholder='password'
            register={register('password', isPasswordValid)}
            error={errors.password && errors.password?.message}
          />
          <AppButton title='Create' type='submit' />
        </form>
      </Modal>
    </>
  );
};
