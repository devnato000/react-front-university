import React from 'react';
import mainRow from './lectorsPageMainRow.module.scss';
import { IRowLectorsData } from './LectorsPage';

export interface IRowProps {
  rowData: IRowLectorsData;
  children: React.ReactNode;
}

export const LectorsPageMainRow: React.FC<IRowProps> = ({ rowData, children }) => {
  return (
    <div id={`${rowData.id}`} className={mainRow['main__row']}>
      <span className={mainRow['main-row__item']}>{rowData.id}</span>
      <span className={mainRow['main-row__item']}>{rowData.name}</span>
      <span className={mainRow['main-row__item']}>{rowData.email}</span>
      {children}
    </div>
  );
};
