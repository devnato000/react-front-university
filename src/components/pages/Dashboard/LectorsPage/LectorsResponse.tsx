import React, { useEffect } from 'react';
import { AxiosResponse } from 'axios';
import { editLector, getLectors } from '../../../../api/lectors.api';
import mainRow from './lectorsPageMainRow.module.scss';
import main from '../../../components/dashboard-page-components/Main/main.module.scss';
import { LectorsPageMainRow } from './LectorsPageMainRow';
import { IAddLectorFormInputs } from './LectorsPage';
import edit from '../../../../images/dashboard-icons/edit.svg';
import Modal from '../../../components/dashboard-page-components/Modal/Modal';
import modal from '../../../components/dashboard-page-components/Modal/modal.module.scss';
import { FormInput } from '../../../components/Inputs/FormInput/FormInput';
import { AppButton } from '../../../components/Buttons/Button';
import { useAppDispatch, useAppSelector } from '../../../../utils/hooks';
import toast from 'react-hot-toast';
import { RootState } from '../../../../redux/store';
import { useForm } from 'react-hook-form';
import { setEditModalState } from '../../../../redux/modalState/modal.slice';
import {
  setCurrentLectorData,
  setLectorData,
  setLectorDataKeys,
} from '../../../../redux/lectors/lectors.slice';

interface ILectorResponse {
  event: (data: IAddLectorFormInputs) => Promise<void>;
}

interface ILectorData {
  id: number;
  name: string;
  email: string;
}

export const LectorResponse: React.FC<ILectorResponse> = (onSubmit) => {
  const lectorsData: ILectorData[] = useAppSelector((state: RootState) => state.lectors.data);
  const lectorsDataKeys = useAppSelector((state: RootState) => state.lectors.dataKeys);
  const currentLectorData = useAppSelector((state: RootState) => state.lectors.currentLectorData);
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<IAddLectorFormInputs>({ mode: 'all' });

  const dispatch = useAppDispatch();
  const notify = (error) => toast.error(`${error}`);

  const isEditModalOpen = useAppSelector((state: RootState) => state.modal.isEditModalOpen);

  const onSubmitEdit = async (data: IAddLectorFormInputs) => {
    try {
      const id = currentLectorData.id;
      const requestData = { ...currentLectorData[0] };
      delete requestData.id;

      if (data.name) {
        requestData.name = data.name;
      }
      if (data.email) {
        requestData.email = data.email;
      }

      await editLector(id, requestData);
      dispatch(setEditModalState(false));
      reset();
    } catch (error: any) {
      setEditModalState(false);
      console.log(error);
      notify(error.response?.data?.message);
    }
  };

  const onCloseEdit = async () => {
    try {
      dispatch(setEditModalState(false));
      reset();
    } catch (error: any) {
      setEditModalState(false);
      notify(error.response?.data?.message);
    }
  };
  const onOpenEdit = async (id) => {
    const currentLector = lectorsData.find((item) => item.id === id);
    dispatch(setCurrentLectorData(currentLector));
    dispatch(setEditModalState(true));
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res: AxiosResponse = await getLectors();
        dispatch(setLectorData(res.data));

        const keys = Object.keys(res.data[0]);
        dispatch(setLectorDataKeys(keys));
      } catch (error: any) {
        console.error('Error fetching data:', error.response?.data?.message);
      }
    };

    fetchData();
  }, [onSubmit, isEditModalOpen]);

  return (
    <>
      <div className={mainRow['main__titles-row']}>
        {lectorsDataKeys.map((item) => {
          return (
            <h3 className={mainRow['main-titles-row__title']} key={item}>
              {item}
            </h3>
          );
        })}
      </div>
      <div className={main['main-row-wrapper']}>
        {lectorsData.map((item) => {
          return (
            <LectorsPageMainRow key={item.id} rowData={item}>
              <button className={mainRow['main-row__edit-btn']} onClick={() => onOpenEdit(item.id)}>
                <img className={mainRow['main-row-edit-btn__image']} src={edit} />
              </button>
            </LectorsPageMainRow>
          );
        })}
        <Modal isOpen={isEditModalOpen} onClose={onCloseEdit}>
          <form onSubmit={handleSubmit(onSubmitEdit)} className={modal['modal-form']}>
            <h2 className={modal['modal-title']}>Edit Lector</h2>
            <FormInput
              label='name'
              type='text'
              placeholder={currentLectorData.name}
              register={register('name', {
                required: false,
              })}
              error={errors.name && errors.name?.message}
            />
            <FormInput
              label='email'
              type='email'
              placeholder={currentLectorData.email}
              register={register('email', {
                required: false,
                pattern: {
                  value: /(\w\.?)+@[\w.-]+\.\w{2,4}/,
                  message: `Please enter valid email`,
                },
              })}
              error={errors.email && errors.email?.message}
            />
            <AppButton title='Change' type='submit' />
          </form>
        </Modal>
      </div>
    </>
  );
};
