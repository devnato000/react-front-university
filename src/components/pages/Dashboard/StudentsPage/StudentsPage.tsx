import React, { useEffect, useState } from 'react';
import toast from 'react-hot-toast';
import { AppHeader } from '../../../components/dashboard-page-components/Header/Header';
import {
  DashboardMain,
  IButtonProps,
} from '../../../components/dashboard-page-components/Main/DashboardMain';
import Modal from '../../../components/dashboard-page-components/Modal/Modal';
import { StudentsResponse } from './StudentsResponse';
import modal from '../../../components/dashboard-page-components/Modal/modal.module.scss';
import { FormInput } from '../../../components/Inputs/FormInput/FormInput';
import { isEmailValid } from '../../../../utils/validation-schema';
import { AppButton } from '../../../components/Buttons/Button';
import { useForm } from 'react-hook-form';
import {
  getStudentsByFilters,
  getStudentsByName,
  postNewStudents,
} from '../../../../api/students.api';
import main from '../../../components/dashboard-page-components/Main/main.module.scss';
import { ISearchInputProps, SearchInput } from '../../../components/Inputs/SearchInput/SearchInput';
import { useAppDispatch } from '../../../../utils/hooks';
import { setStudentData } from '../../../../redux/students/students.slice';
import { AppSelect, Option } from '../../../components/Select/AppSelect';

interface IStudentsPageProps {
  title: string;
}

export interface IAddStudentsFormInputs {
  name: string;
  surname: string;
  email: string;
  age: number;
  imagePath: string;
  groupId: number;
}

export const StudentsPage: React.FC<IStudentsPageProps> = (StudentsPageProps) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [name, setName] = useState('');

  const dispatch = useAppDispatch();

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const options: Option[] = [
    { value: ['createdAt', 'DESC'], label: 'createdAt desc' },
    { value: ['createdAt', 'ASC'], label: 'createdAt asc' },
    { value: ['name', 'DESC'], label: 'name desc' },
    { value: ['name', 'ASC'], label: 'name asc' },
    { value: ['surname', 'DESC'], label: 'surname desc' },
    { value: ['surname', 'ASC'], label: 'surname asc' },
    { value: ['age', 'DESC'], label: 'age desc' },
    { value: ['age', 'ASC'], label: 'age asc' },
  ];

  const handleFilter = async (selectedOption) => {
    try {
      const [filter, order] = selectedOption.value;
      const response = await getStudentsByFilters(filter, order);
      dispatch(setStudentData(response.data));
    } catch (error) {
      console.error('Error fetching students:', error);
    }
  };

  const inputProps: ISearchInputProps = {
    type: 'search',
    id: 'search',
    placeholder: 'Search',
    onChange: (e) => setName(e.target.value),
  };

  const buttonProps: IButtonProps = {
    title: '+ Add new Student',
    type: 'submit',
    onClick: openModal,
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IAddStudentsFormInputs>({ mode: 'all' });

  const notify = (error) => toast.error(`${error}`);

  const onSubmit = async (data: IAddStudentsFormInputs) => {
    try {
      await postNewStudents({
        ...data,
        age: Number(data.age),
      });
    } catch (error: any) {
      notify(error.response?.data?.message);
    }
  };

  const handleSearch = async () => {
    try {
      const response = await getStudentsByName(name);
      dispatch(setStudentData(response.data));
    } catch (error) {
      console.error('Error fetching students:', error);
    }
  };
  useEffect(() => {
    handleSearch();
  }, [name]);

  return (
    <>
      <AppHeader title={StudentsPageProps.title} />
      <DashboardMain>
        <>
          <label htmlFor='sort' className={main['dashboard-controls__label']}>
            sort by
          </label>
          <AppSelect styles='dashboard' options={options} onChange={handleFilter} />
          <SearchInput {...inputProps} />
          <AppButton
            title={buttonProps.title}
            type={buttonProps.type}
            onClick={buttonProps.onClick}
          />
        </>
        <StudentsResponse event={onSubmit} />
      </DashboardMain>
      <Modal isOpen={isModalOpen} onClose={closeModal}>
        <form onSubmit={handleSubmit(onSubmit)} className={modal['modal-form']}>
          <h2 className={modal['modal-title']}>Add New Student</h2>
          <FormInput
            label='name'
            type='text'
            placeholder='John'
            register={register('name', {
              required: {
                value: true,
                message: 'This field is required',
              },
            })}
            error={errors.name && errors.name?.message}
          />
          <FormInput
            label='surname'
            type='text'
            placeholder='Smith'
            register={register('surname', {
              required: {
                value: true,
                message: 'This field is required',
              },
            })}
            error={errors.surname && errors.surname?.message}
          />
          <FormInput
            label='email'
            type='email'
            placeholder='example@mail.com'
            register={register('email', isEmailValid)}
            error={errors.email && errors.email?.message}
          />
          <FormInput
            label='age'
            type='number'
            placeholder='age'
            register={register('age', {
              required: {
                value: true,
                message: 'This field is required',
              },
            })}
            error={errors.age && errors.age?.message}
          />
          <FormInput
            label='group id'
            type='number'
            placeholder='group id'
            register={register('groupId')}
            error={errors.groupId && errors.groupId?.message}
          />
          <FormInput
            label='image'
            type='text'
            placeholder='image'
            register={register('imagePath')}
            error={errors.imagePath && errors.imagePath?.message}
          />
          <AppButton title='Create' type='submit' />
        </form>
      </Modal>
    </>
  );
};
