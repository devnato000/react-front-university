import React, { useEffect } from 'react';
import { AxiosResponse } from 'axios';
import { getStudents } from '../../../../api/students.api';
import { IAddStudentsFormInputs } from './StudentsPage';
import { useAppDispatch, useAppSelector } from '../../../../utils/hooks';
import { RootState } from '../../../../redux/store';
import toast from 'react-hot-toast';
import {
  setCurrentStudentData,
  setStudentData,
  setStudentDataKeys,
} from '../../../../redux/students/students.slice';
import { useNavigate } from 'react-router-dom';
import { AppTable } from '../../../components/dashboard-page-components/Table/Table';
import { StudentsPageMainRow } from './StudentsPageMainRow';

interface IStudentResponse {
  event: (data: IAddStudentsFormInputs) => Promise<void>;
}

export interface IStudentData {
  id: number;
  name: string;
  surname: string;
  email: string;
  age: number;
  imagePath: string;
  group?: {
    id: number;
    name: string;
  };
}

const header = [
  {
    name: '',
  },
  {
    name: 'name',
  },
  {
    name: 'surname',
  },
  {
    name: 'email',
  },
  {
    name: 'age',
  },
  {
    name: 'group',
  },
];

const gridTemplate = '80px 1fr 1fr 1.5fr 0.5fr 2fr 25px';

export const StudentsResponse: React.FC<IStudentResponse> = (onSubmit) => {
  const studentsData: IStudentData[] = useAppSelector((state: RootState) => state.students.data);

  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const notify = (error) => toast.error(`${error}`);

  const isEditModalOpen = useAppSelector((state: RootState) => state.modal.isEditModalOpen);

  const onOpenEdit = async (id) => {
    const currentStudent = studentsData.find((item) => item.id === id);

    dispatch(setCurrentStudentData(currentStudent));

    navigate(`/profile/${id}`);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res: AxiosResponse = await getStudents();
        dispatch(setStudentData(res.data));

        const keys = Object.keys(res.data[0]);
        dispatch(setStudentDataKeys(keys));
      } catch (error: any) {
        console.error('Error fetching data:', error.response?.data?.message);
        notify(error.response?.data?.message);
      }
    };

    fetchData();
  }, [onSubmit, isEditModalOpen]);

  return (
    <>
      <AppTable header={header} gridTemplate={gridTemplate}>
        {studentsData.map((item) => (
          <StudentsPageMainRow
            key={item.id}
            rowData={item}
            gridTemplate={gridTemplate}
            onClick={onOpenEdit}
          ></StudentsPageMainRow>
        ))}
      </AppTable>
    </>
  );
};
