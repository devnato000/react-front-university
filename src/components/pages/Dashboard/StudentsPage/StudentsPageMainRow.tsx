import React from 'react';
import mainRow from '../../../components/dashboard-page-components/Main/mainRow.module.scss';
import { IStudentData } from './StudentsResponse';
import edit from '../../../../images/dashboard-icons/edit.svg';

export interface IRowProps {
  rowData: IStudentData;
  onClick: (id) => Promise<void>;
  gridTemplate: string;
}

export const StudentsPageMainRow: React.FC<IRowProps> = ({ rowData, gridTemplate, onClick }) => {
  const rowStyles = {
    gridTemplateColumns: gridTemplate,
  };
  const image = `https://university-api-nlfn.onrender.com/uploads/${rowData.imagePath}`;

  return (
    <div className={mainRow['main__row']} style={rowStyles}>
      <span className={mainRow['main-row__item']}>
        {rowData.imagePath ? <img src={image} className={mainRow['main-row-item__image']} /> : null}
      </span>
      <span className={mainRow['main-row__item']}>{rowData.name}</span>
      <span className={mainRow['main-row__item']}>{rowData.surname}</span>
      <span className={mainRow['main-row__item']}>{rowData.email}</span>
      <span className={mainRow['main-row__item']}>{rowData.age}</span>
      <span className={mainRow['main-row__item']}>{rowData.group?.name}</span>
      <button className={mainRow['main-row__edit-btn']} onClick={() => onClick(rowData.id)}>
        <img className={mainRow['main-row-edit-btn__image']} src={edit} />
      </button>
    </div>
  );
};
