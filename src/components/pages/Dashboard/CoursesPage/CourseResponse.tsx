import React, { useEffect } from 'react';
import { AxiosResponse } from 'axios';
import { editCourse, getCourses } from '../../../../api/courses.api';
import mainRow from './coursePageMainRow.module.scss';
import main from '../../../components/dashboard-page-components/Main/main.module.scss';
import { CoursePageMainRow } from './CoursePageMainRow';
import { IAddCourseFormInputs } from './CoursesPage';
import edit from '../../../../images/dashboard-icons/edit.svg';
import Modal from '../../../components/dashboard-page-components/Modal/Modal';
import modal from '../../../components/dashboard-page-components/Modal/modal.module.scss';
import { FormInput } from '../../../components/Inputs/FormInput/FormInput';
import { AppButton } from '../../../components/Buttons/Button';
import { useAppDispatch, useAppSelector } from '../../../../utils/hooks';
import { RootState } from '../../../../redux/store';
import { useForm } from 'react-hook-form';
import toast from 'react-hot-toast';
import { setEditModalState } from '../../../../redux/modalState/modal.slice';
import {
  setCourseData,
  setCourseDataKeys,
  setCurrentCourseData,
} from '../../../../redux/courses/courses.slice';

interface ICourseResponse {
  event: (data: IAddCourseFormInputs) => Promise<void>;
}

export interface ICourseData {
  id: number;
  name: string;
  hours: number;
  description: string;
}

export const CourseResponse: React.FC<ICourseResponse> = (onSubmit) => {
  const courseData: ICourseData[] = useAppSelector((state: RootState) => state.courses.data);
  const courseDataKeys = useAppSelector((state: RootState) => state.courses.dataKeys);
  const currentCourseData = useAppSelector((state: RootState) => state.courses.currentCourseData);
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<ICourseData>({ mode: 'all' });

  const dispatch = useAppDispatch();
  const notify = (error) => toast.error(`${error}`);

  const isEditModalOpen = useAppSelector((state: RootState) => state.modal.isEditModalOpen);

  const onCloseEdit = async () => {
    try {
      dispatch(setEditModalState(false));
      reset();
    } catch (error: any) {
      setEditModalState(false);
      notify(error.response?.data?.message);
    }
  };
  const onOpenEdit = async (id) => {
    const currentLector = courseData.find((item) => item.id === id);

    dispatch(setCurrentCourseData(currentLector));
    dispatch(setEditModalState(true));
  };

  const onSubmitEdit = async (data: ICourseData) => {
    try {
      const id = currentCourseData.id;
      const requestData = { ...currentCourseData[0] };
      delete requestData.id;
      if (data.name) {
        requestData.name = data.name;
      }
      if (data.hours) {
        requestData.hours = data.hours;
      }
      if (data.description) {
        requestData.description = data.description;
      }
      await editCourse(id, requestData);
      dispatch(setEditModalState(false));
      reset();
    } catch (error: any) {
      setEditModalState(false);
      console.log(error);
      notify(error.response?.data?.message);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res: AxiosResponse = await getCourses();
        dispatch(setCourseData(res.data));

        const keys = Object.keys(res.data[0]);
        dispatch(setCourseDataKeys(keys));
      } catch (error: any) {
        console.error('Error fetching data:', error.response?.data?.message);
      }
    };

    fetchData().then((r) => r);
  }, [onSubmit, isEditModalOpen]);

  return (
    <>
      <div className={mainRow['main__titles-row']}>
        {courseDataKeys.map((item) => (
          <h3 className={mainRow['main-titles-row__title']} key={item}>
            {item}
          </h3>
        ))}
      </div>
      <div className={main['main-row-wrapper']}>
        {courseData.map((item) => (
          <CoursePageMainRow key={item.id} rowData={item}>
            <button className={mainRow['main-row__edit-btn']} onClick={() => onOpenEdit(item.id)}>
              <img className={mainRow['main-row-edit-btn__image']} src={edit} />
            </button>
          </CoursePageMainRow>
        ))}
        <Modal isOpen={isEditModalOpen} onClose={onCloseEdit}>
          <form onSubmit={handleSubmit(onSubmitEdit)} className={modal['modal-form']}>
            <h2 className={modal['modal-title']}>Edit Lector</h2>
            <FormInput
              label='name'
              type='text'
              placeholder={currentCourseData.name}
              register={register('name', {
                required: false,
              })}
              error={errors.name && errors.name?.message}
            />
            <FormInput
              label='hours'
              type='number'
              placeholder={String(currentCourseData.hours)}
              register={register('hours')}
              error={errors.hours && errors.hours?.message}
            />
            <FormInput
              label='description'
              type='text'
              placeholder={currentCourseData.description}
              register={register('description')}
              error={errors.description && errors.description?.message}
            />
            <AppButton title='Change' type='submit' />
          </form>
        </Modal>
      </div>
    </>
  );
};
