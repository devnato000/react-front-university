import React from 'react';
import mainRow from './coursePageMainRow.module.scss';
import { ICourseData } from './CourseResponse';

export interface IRowProps {
  rowData: ICourseData;
  children: React.ReactNode;
}

export const CoursePageMainRow: React.FC<IRowProps> = ({ rowData, children }) => {
  return (
    <div className={mainRow['main__row']}>
      <span className={mainRow['main-row__item']}>{rowData.id}</span>
      <span className={mainRow['main-row__item']}>{rowData.name}</span>
      <span className={mainRow['main-row__item']}>{rowData.description}</span>
      <span className={mainRow['main-row__item']}>{rowData.hours}</span>
      {children}
    </div>
  );
};
