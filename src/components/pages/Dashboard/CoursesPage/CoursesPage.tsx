import React, { useState } from 'react';
import toast from 'react-hot-toast';
import {
  DashboardMain,
  IButtonProps,
} from '../../../components/dashboard-page-components/Main/DashboardMain';
import { postNewCourse } from '../../../../api/courses.api';
import { AppHeader } from '../../../components/dashboard-page-components/Header/Header';
import Modal from '../../../components/dashboard-page-components/Modal/Modal';
import modal from '../../../components/dashboard-page-components/Modal/modal.module.scss';
import { FormInput } from '../../../components/Inputs/FormInput/FormInput';
import { useForm } from 'react-hook-form';
import { CourseResponse } from './CourseResponse';
import { AppButton } from '../../../components/Buttons/Button';
import main from '../../../components/dashboard-page-components/Main/main.module.scss';
import { AppSelect, Option } from '../../../components/Select/AppSelect';
import { ISearchInputProps, SearchInput } from '../../../components/Inputs/SearchInput/SearchInput';

interface ICoursesPageProps {
  title: string;
}

export interface IAddCourseFormInputs {
  name: string;
  description: string;
  hours: number;
}

export const CoursesPage: React.FC<ICoursesPageProps> = (CoursesPageProps) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
    reset();
  };

  const inputProps: ISearchInputProps = {
    type: 'search',
    id: 'search',
    placeholder: 'Search',
  };
  const buttonProps: IButtonProps = {
    title: '+ Add new Course',
    type: 'submit',
    onClick: openModal,
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<IAddCourseFormInputs>({ mode: 'all' });

  const notify = (error) => toast.error(`${error}`);

  const onSubmit = async (data: IAddCourseFormInputs) => {
    try {
      await postNewCourse(data);
    } catch (error: any) {
      notify(error.response?.data?.message);
    }
  };
  const options: Option[] = [
    { value: 1, label: 'Option 1' },
    { value: 2, label: 'Option 2' },
  ];
  return (
    <>
      <AppHeader title={CoursesPageProps.title} />
      <DashboardMain>
        <>
          <label htmlFor='sort' className={main['dashboard-controls__label']}>
            sort by
          </label>
          <AppSelect styles='dashboard' isMulti options={options} />
          <SearchInput {...inputProps} />
          <AppButton
            title={buttonProps.title}
            type={buttonProps.type}
            onClick={buttonProps.onClick}
          />
        </>
        <CourseResponse event={onSubmit} />
      </DashboardMain>
      <Modal isOpen={isModalOpen} onClose={closeModal}>
        <form onSubmit={handleSubmit(onSubmit)} className={modal['modal-form']}>
          <h2 className={modal['modal-title']}>Add New Course</h2>
          <FormInput
            label='Course name'
            type='text'
            placeholder='Course'
            register={register('name', {
              required: {
                value: true,
                message: 'This field is required',
              },
            })}
            error={errors.name && errors.name?.message}
          />
          <FormInput
            label='description'
            type='textarea'
            placeholder='description'
            register={register('description', {
              required: {
                value: true,
                message: 'This field is required',
              },
            })}
            error={errors.description && errors.description?.message}
          />
          <FormInput
            label='hours'
            type='number'
            placeholder='hours'
            register={register('hours', {
              required: {
                value: true,
                message: 'This field is required',
              },
            })}
            error={errors.hours && errors.hours?.message}
          />
          <AppButton title='Create' type='submit' />
        </form>
      </Modal>
    </>
  );
};
