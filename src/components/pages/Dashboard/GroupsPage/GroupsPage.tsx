import React, { useState } from 'react';
import toast from 'react-hot-toast';
import { AppHeader } from '../../../components/dashboard-page-components/Header/Header';
import {
  DashboardMain,
  IButtonProps,
} from '../../../components/dashboard-page-components/Main/DashboardMain';
import { postNewGroup } from '../../../../api/groups.api';
import Modal from '../../../components/dashboard-page-components/Modal/Modal';
import { GroupsResponse } from './GroupsResponse';
import modal from '../../../components/dashboard-page-components/Modal/modal.module.scss';
import { FormInput } from '../../../components/Inputs/FormInput/FormInput';
import { AppButton } from '../../../components/Buttons/Button';
import { useForm } from 'react-hook-form';
import main from '../../../components/dashboard-page-components/Main/main.module.scss';
import { AppSelect, Option } from '../../../components/Select/AppSelect';
import { ISearchInputProps, SearchInput } from '../../../components/Inputs/SearchInput/SearchInput';

interface IGroupsPageProps {
  title: string;
}

export interface IAddGroupFormInputs {
  name: string;
}

export const GroupsPage: React.FC<IGroupsPageProps> = (GroupsPageProps) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => {
    setIsModalOpen(true);
  };
  const closeModal = () => {
    setIsModalOpen(false);
  };

  const inputProps: ISearchInputProps = {
    type: 'search',
    id: 'search',
    placeholder: 'Search',
  };
  const buttonProps: IButtonProps = {
    title: '+ Add new Course',
    type: 'submit',
    onClick: openModal,
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IAddGroupFormInputs>({ mode: 'all' });

  const notify = (error) => toast.error(`${error}`);

  const onSubmit = async (data: IAddGroupFormInputs) => {
    try {
      await postNewGroup(data);
    } catch (error: any) {
      notify(error.response?.data?.message);
    }
  };
  const options: Option[] = [
    { value: 1, label: 'Option 1' },
    { value: 2, label: 'Option 2' },
  ];
  return (
    <>
      <AppHeader title={GroupsPageProps.title} />
      <DashboardMain>
        <>
          <label htmlFor='sort' className={main['dashboard-controls__label']}>
            sort by
          </label>
          <AppSelect styles='dashboard' isMulti options={options} />
          <SearchInput {...inputProps} />
          <AppButton
            title={buttonProps.title}
            type={buttonProps.type}
            onClick={buttonProps.onClick}
          />
        </>
        <GroupsResponse event={onSubmit} />
      </DashboardMain>
      <Modal isOpen={isModalOpen} onClose={closeModal}>
        <form onSubmit={handleSubmit(onSubmit)} className={modal['modal-form']}>
          <h2 className={modal['modal-title']}>Add New Group</h2>
          <FormInput
            label='name'
            type='text'
            placeholder='Group'
            register={register('name', {
              required: {
                value: true,
                message: 'This field is required',
              },
            })}
            error={errors.name && errors.name?.message}
          />
          <AppButton title='Create' type='submit' />
        </form>
      </Modal>
    </>
  );
};
