import React, { useEffect } from 'react';
import { AxiosResponse } from 'axios';
import mainRow from './groupsPageMainRow.module.scss';
import main from '../../../components/dashboard-page-components/Main/main.module.scss';
import { GroupsPageMainRow } from './GroupsPageMainRow';
import { IAddGroupFormInputs } from './GroupsPage';
import edit from '../../../../images/dashboard-icons/edit.svg';
import Modal from '../../../components/dashboard-page-components/Modal/Modal';
import modal from '../../../components/dashboard-page-components/Modal/modal.module.scss';
import { FormInput } from '../../../components/Inputs/FormInput/FormInput';
import { AppButton } from '../../../components/Buttons/Button';
import { useAppDispatch, useAppSelector } from '../../../../utils/hooks';
import { RootState } from '../../../../redux/store';
import { useForm } from 'react-hook-form';
import toast from 'react-hot-toast';
import { setEditModalState } from '../../../../redux/modalState/modal.slice';
import {
  setCurrentGroupData,
  setGroupData,
  setGroupDataKeys,
} from '../../../../redux/groups/groups.slice';
import { editGroup, getGroups } from '../../../../api/groups.api';

interface IGroupResponse {
  event: (data: IAddGroupFormInputs) => Promise<void>;
}

export interface IGroupData {
  id: number;
  name: string;
}

export const GroupsResponse: React.FC<IGroupResponse> = (onSubmit) => {
  const groupsData: IGroupData[] = useAppSelector((state: RootState) => state.groups.data);
  const groupsDataKeys = useAppSelector((state: RootState) => state.groups.dataKeys);
  const currentLectorData = useAppSelector((state: RootState) => state.groups.currentGroupData);
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<IGroupData>({ mode: 'all' });

  const dispatch = useAppDispatch();
  const notify = (error) => toast.error(`${error}`);

  const isEditModalOpen = useAppSelector((state: RootState) => state.modal.isEditModalOpen);

  const onSubmitEdit = async (data: IGroupData) => {
    try {
      const id = currentLectorData.id;
      const requestData = { ...currentLectorData[0] };
      delete requestData.id;

      if (data.name) {
        requestData.name = data.name;
      }

      await editGroup(id, requestData);
      dispatch(setEditModalState(false));
      reset();
    } catch (error: any) {
      setEditModalState(false);
      console.log(error);
      notify(error.response?.data?.message);
    }
  };

  const onCloseEdit = async () => {
    try {
      dispatch(setEditModalState(false));
      reset();
    } catch (error: any) {
      setEditModalState(false);
      notify(error.response?.data?.message);
    }
  };
  const onOpenEdit = async (id) => {
    const currentLector = groupsData.find((item) => item.id === id);

    dispatch(setCurrentGroupData(currentLector));
    dispatch(setEditModalState(true));
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res: AxiosResponse = await getGroups();
        dispatch(setGroupData(res.data));

        const keys = Object.keys(res.data[0]);
        dispatch(setGroupDataKeys(keys));
      } catch (error: any) {
        console.error('Error fetching data:', error.response?.data?.message);
      }
    };

    fetchData();
  }, [onSubmit, isEditModalOpen]);

  return (
    <>
      <div className={mainRow['main__titles-row']}>
        {groupsDataKeys.map((item) => (
          <h3 className={mainRow['main-titles-row__title']} key={item}>
            {item}
          </h3>
        ))}
      </div>
      <div className={main['main-row-wrapper']}>
        {groupsData.map((item) => (
          <GroupsPageMainRow key={item.id} rowData={item}>
            <button className={mainRow['main-row__edit-btn']} onClick={() => onOpenEdit(item.id)}>
              <img className={mainRow['main-row-edit-btn__image']} src={edit} />
            </button>
          </GroupsPageMainRow>
        ))}
        <Modal isOpen={isEditModalOpen} onClose={onCloseEdit}>
          <form onSubmit={handleSubmit(onSubmitEdit)} className={modal['modal-form']}>
            <h2 className={modal['modal-title']}>Edit Lector</h2>
            <FormInput
              label='name'
              type='text'
              placeholder={currentLectorData.name}
              register={register('name', {
                required: false,
              })}
              error={errors.name && errors.name?.message}
            />
            <AppButton title='Change' type='submit' />
          </form>
        </Modal>
      </div>
    </>
  );
};
