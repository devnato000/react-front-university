import React from 'react';
import mainRow from './groupsPageMainRow.module.scss';
import { IGroupData } from './GroupsResponse';

export interface IRowProps {
  rowData: IGroupData;
  children: React.ReactNode;
}

export const GroupsPageMainRow: React.FC<IRowProps> = ({ rowData, children }) => {
  return (
    <div className={mainRow['main__row']}>
      <span className={mainRow['main-row__item']}>{rowData.id}</span>
      <span className={mainRow['main-row__item']}>{rowData.name}</span>
      {children}
    </div>
  );
};
