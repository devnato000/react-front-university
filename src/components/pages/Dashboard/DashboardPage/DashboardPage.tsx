import React from 'react';
import { DashboardMain } from '../../../components/dashboard-page-components/Main/DashboardMain';
import { AppHeader } from '../../../components/dashboard-page-components/Header/Header';

interface IDashboardPageProps {
  title: string;
}

export interface IData {
  id: number;
  name: string;
  email: string;
  courses?: [];
  marks?: [];
}

export const DashboardPage: React.FC<IDashboardPageProps> = (DashboardPageProps) => {
  return (
    <>
      <AppHeader title={DashboardPageProps.title} />
      <DashboardMain>{/*<Response/>*/}</DashboardMain>
    </>
  );
};
