import React from 'react';
import {IResetForm, ResetForm} from "../../Forms/ResetForm/ResetForm";
import AuthLayout from "../../pageLayouts/AuthPageLayout/AuthPageLayout";


export const ResetPassPage = () => {
	const ResetPassPageProps: IResetForm = {
		buttonTitle: 'Reset'
	}
	const title = 'Reset Password'
	
	return (
		<AuthLayout title={title}>
			<ResetForm {...ResetPassPageProps}/>
		</AuthLayout>
	
	)
}