import React, { useEffect, useState } from 'react';
import { AppHeader } from '../../components/dashboard-page-components/Header/Header';
import profile from './profile.module.scss';
import plusIcon from '../../../images/dashboard-icons/plus.svg';
import backIcon from '../../../images/dashboard-icons/back-arrow.svg';
import { Controller, useForm } from 'react-hook-form';
import toast from 'react-hot-toast';
import { FormInput } from '../../components/Inputs/FormInput/FormInput';
import { AppButton } from '../../components/Buttons/Button';
import { AppSelect } from '../../components/Select/AppSelect';
import { useNavigate } from 'react-router-dom';
import { RootState } from '../../../redux/store';
import { useAppDispatch, useAppSelector } from '../../../utils/hooks';
import {
  addGroupToStudent,
  editStudent,
  getStudentImage,
  getStudents,
  uploadImageStudent,
} from '../../../api/students.api';
import { getGroups } from '../../../api/groups.api';
import { setGroupData } from '../../../redux/groups/groups.slice';
import { IGroupData } from '../Dashboard/GroupsPage/GroupsResponse';
import { ICourseData } from '../Dashboard/CoursesPage/CourseResponse';
import { getCourses } from '../../../api/courses.api';
import { setCourseData } from '../../../redux/courses/courses.slice';
import { setStudentData } from '../../../redux/students/students.slice';

interface IProfileFormInputs {
  image?: File | null;
  name: string;
  surname: string;
  email: string;
  age: number;
  course?: string;
  group?: Option | null;
}

interface Option {
  value: number;
  label: string;
}

export const ProfilePage: React.FC = () => {
  const currentStudentData = useAppSelector(
    (state: RootState) => state.students.currentStudentData,
  );
  const groupsData: IGroupData[] = useAppSelector((state: RootState) => state.groups.data);
  const courseData: ICourseData[] = useAppSelector((state: RootState) => state.courses.data);

  const [isDisabled, setIsDisabled] = useState(true);
  const [selectedFile, setSelectedFile] = useState(null);
  const [imagePath, setImagePath] = useState();

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    control,
    setValue,
    formState: { errors },
  } = useForm<IProfileFormInputs>({ mode: 'all' });

  const notify = (error) => toast.error(`${error}`);
  const notifyAsync = (myPromise) =>
    toast.promise(myPromise, {
      loading: 'Loading',
      success: 'Got the data',
      error: 'Error when fetching',
    });

  const goBack = () => {
    navigate(-1);
  };

  const handleChange = async (event) => {
    setSelectedFile(event.target.files[0]);
  };

  const checkImage = async () => {
    const id = window.location.pathname.split('/')[2];

    const image = await getStudentImage(id);
    setImagePath(image.data);
  };

  const replaceImage = async (id, imageData) => {
    try {
      await notifyAsync(uploadImageStudent(id, imageData));
    } catch (error) {
      console.log(error);
      notify(error);
    }
  };

  const image = `https://university-api-nlfn.onrender.com/uploads/${imagePath}`;

  const imageBackground = {
    backgroundImage: `url(${image})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  };

  const onSubmit = async (data: IProfileFormInputs) => {
    try {
      const id = window.location.pathname.split('/')[2];
      if (data.age) {
        data.age = Number(data.age);
      }

      if (selectedFile) {
        const imageData = new FormData();
        imageData.append('file', selectedFile);
        await replaceImage(id, imageData);
      }
      delete data.image;

      if (data.group) {
        const requestGroupData = {
          group: String(data.group),
        };
        await addGroupToStudent(id, requestGroupData);
      }
      delete data.group;

      const filledFields = Object.entries(data).reduce((acc, [key, value]) => {
        if (value !== undefined && value !== '') {
          acc[key] = value;
        }
        return acc;
      }, {});

      notifyAsync(editStudent(id, filledFields));
    } catch (error) {
      console.log(error);
      notify(error);
    }
  };

  const coursesOptions: Option[] = courseData.map((course) => ({
    value: course.id,
    label: course.name,
  }));
  const groupsOptions: Option[] = groupsData.map((group) => ({
    value: group.id,
    label: group.name,
  }));

  useEffect(() => {
    const fetchData = async () => {
      try {
        const students = await getStudents();
        dispatch(setStudentData(students.data));

        const groups = await getGroups();
        dispatch(setGroupData(groups.data));

        const courses = await getCourses();
        dispatch(setCourseData(courses.data));

        setIsDisabled(false);
      } catch (error) {
        console.log(error);
        notify(error);
      }
    };
    fetchData();
  }, []);

  return (
    <>
      <AppHeader />
      <div className={profile['profile-wrapper']}>
        <button className={profile['profile-back-button']} onClick={goBack}>
          <img src={backIcon} className={profile['profile-back-button__icon']} />
          Back
        </button>
        <form onSubmit={handleSubmit(onSubmit)} className={profile['profile-form']}>
          <div className={profile['profile-form__left']}>
            <h2 className={profile['profile-form__title']}>Personal information</h2>
            <div className={profile['profile-form__image-replace']}>
              <div className={profile['profile-form-image-replace__left']}>
                <label
                  className={profile['profile-form-image-replace__image-pick-label']}
                  style={imageBackground}
                >
                  {imagePath ? (
                    <></>
                  ) : (
                    <img
                      className={profile['profile-form-image-replace__image-pick-label__icon']}
                      src={plusIcon}
                    />
                  )}
                  <input
                    type='file'
                    accept='image/png, image/jpeg'
                    className={profile['profile-form-image-replace__image-pick']}
                    id='image'
                    {...register('image', {
                      onChange: (e) => handleChange(e),
                    })}
                  />
                </label>
              </div>
              <div className={profile['profile-form-image-replace__right']}>
                <AppButton
                  buttonClass={profile['profile-form-image-replace-right__button']}
                  title='Replace'
                  disabled={isDisabled}
                  type='button'
                  onClick={checkImage}
                />
                {isDisabled ? <span>No file chosen</span> : null}

                <p className={profile['profile-form-image-replace-right__description']}>
                  Must be a .jpg or .png file smaller than 10MB and at least 400px by 400px.
                </p>
              </div>
            </div>
            <FormInput
              label='name'
              type='text'
              placeholder={currentStudentData.name}
              register={register('name')}
              error={errors.name && errors.name?.message}
            />
            <FormInput
              label='surname'
              type='text'
              placeholder={currentStudentData.surname}
              register={register('surname')}
              error={errors.surname && errors.surname?.message}
            />
            <FormInput
              label='email'
              type='email'
              placeholder={currentStudentData.email}
              register={register('email')}
              error={errors.email && errors.email?.message}
            />
            <FormInput
              label='age'
              type='number'
              placeholder='age'
              defaultValue={currentStudentData.age}
              register={register('age')}
              error={errors.age && errors.age?.message}
            />
          </div>
          <div className={profile['profile-form__right']}>
            <h2 className={profile['profile-form__title']}>Courses and Groups</h2>
            <label className={profile['profile-form__select-label']}>
              course
              <AppSelect
                className={profile['profile-form__select']}
                styles='profile'
                isMulti={true}
                options={coursesOptions}
              />
            </label>
            <label className={profile['profile-form__select-label']}>
              group
              <Controller
                name='group'
                control={control}
                defaultValue={null}
                render={({ field: { ref } }) => (
                  <AppSelect
                    inputRef={ref}
                    className={profile['profile-form__select']}
                    styles='profile'
                    options={groupsOptions}
                    onChange={(value) => setValue('group', value.value)}
                  />
                )}
              />
            </label>
          </div>
          <div className={profile['profile-form__left']}>
            <AppButton title='Save changes' type='submit' />
          </div>
        </form>
      </div>
    </>
  );
};
